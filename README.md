// show database

Show dbs


// create, use database

use databaseName


// create a collection. A new database will only be created after we create at least a collection/table

db.createCollection(‘collectionTable1’)


// show collections

show collections


// drop a collection/table

db.collectionTable1.drop()


// insert data into a collection

db.nameCollection.insert({name: “Janeiro/17”, month: 1, year: 2017})


// save: insert or update data into a collection

db.nameCollection.save({name: “Fevereiro/17”, month:2, year: 2017})


// add more data into a insert, start with ({ and them ENTER

db.monthsCollection.inser({

… name: “March/17”

… month: 3

… year: 2017

… creadits: [

… {name: “Salario”, value: 5000}

],

… debts: [

… {name: “Luz”, value: 100, status: “PAGO”},

… {name: “Telefone”, value: 100, status: “PENDENTE"}

… ]

… })



// querying all data

db.monthsCollection.find()


// showing all data in a structured way

db.monthsCollection.find().pretty


// show 1 register

db.monthsCollection.findOne()


// show register with filter

db.monthsCollection.findOne({month: 2})


// OR, AND filtering

db.monthsCollection.find({$or: [{month: 1}, {month: 2}]}).pretty()


// if register exists

db.monthsCollections.find({credits: {$exists: true}}).pretty


// paginacao, pula 1 register

db.monthsCollection.find({year: 2017}).skip(1)


// skip register and limit results

db.monthsCollection.find({year: 2017}).skip(1).limit(1)


// agregacao/joins

db.monthsCollection.aggregate([{
    $project:{credit: {$sum: "$credits.value"}, debt: {$sum: "$debts.value"}},
}, {
    $group: {
        _id: null, credit: {$sum: "$credit"}, debt: {$sum: $debt}
    }
}

])


More:
https://blog.codecentric.de/files/2012/12/MongoDB-CheatSheet-v1_0.pdf